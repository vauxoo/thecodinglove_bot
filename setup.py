from setuptools import setup, find_packages

with open('requirements.txt', 'r') as frequ:
    requirements = [pack for pack in frequ]

setup(
    name="TheCodingLoveBot",
    version='1.0.6',
    packages=find_packages(),
    install_requires=requirements,
    entry_points='''
        [console_scripts]
        tcl_bot=tcl_bot.bot:main_loop
    '''
)
