#!/usr/bin/env python
# This example show how to write an inline mode telegramt bot use pyTelegramBotAPI.
import telebot
from telebot import types
import time
import os
import logging
import click
from pymongo import MongoClient


bot = telebot.TeleBot(os.environ.get('TELEGRAM_TOKEN'))
mserver = 'm01'
mport = 27017


def get_results(query, mserver, mport, limit=10):
    mongourl = 'mongodb://%s:%s/' % (mserver, mport)
    client = MongoClient(mongourl)
    db = client['thecodinglove']
    collection = db['posts']
    collection.ensure_index([
        ('title', 'text'),
    ],
        name="search_index",
        weights={
                'title': 100,
            }
    )
    text_results = db.command('text', 'posts', search=query, limit=limit)
    res = []
    identifier = 1
    for ele in text_results['results']:
        title = ele['obj']['title']
        gif = ele['obj']['gif']
        r = types.InlineQueryResultArticle(str(identifier),
                                           title,
                                           '%s\n%s' % (title, gif))
        identifier += 1
        res.append(r)
    if not res:
        telebot.logger.info('Nothing found with search %s' % query)
        return [types.InlineQueryResultArticle('0', 'Nothing Found', 'go to lgtfy.com')]
    return res


@bot.inline_handler(lambda query: len(query.query) is not 0)
def query_text(inline_query):
    try:
        results = get_results(inline_query.query, mserver, mport)
        bot.answer_inline_query(inline_query.id, results)
    except Exception as e:
        telebot.logger.error('I failed on query text %s' % e)

@bot.inline_handler(lambda query: len(query.query) is 0)
def default_query(inline_query):
    try:
        r = types.InlineQueryResultArticle('1', 'default', 'default')
        bot.answer_inline_query(inline_query.id, [r])
    except Exception as e:
        telebot.logger.error('I failed on default query %s' % e)

@click.command()
@click.option('--m-server', help='Mongo server hostname.')
@click.option('--m-port', help='Mongo server port.')
@click.option('--debug', is_flag=True,
              help='Debug mode')
def main_loop(m_server, m_port, debug):
    """Main telegram bot loop set the options to start to serve."""
    click.echo('Starting bot')
    global mserver, mport
    # API_TOKEN = '218869103:AAGPxnA_Ln5GVmCQA6Pape-m3gP_eWIuK68'
    if m_server:
        mserver = m_server
    if m_port:
        mport = int(m_port)
    else:
        if not os.environ.get('TELEGRAM_TOKEN'):
            click.echo('No token please set one on environment '
                       'TELEGRAM_TOKEN or pass as parameter')
            exit()
    telebot.logger.info('Starting bot reading from this mongo server:port %s:%s ' % (mserver, str(mport)))
    if debug:
        telebot.logger.setLevel(logging.DEBUG)
    bot.polling(True)
    while 1:
        time.sleep(3)
