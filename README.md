[![Build Status](https://travis-ci.org/nhomar/thecodinglove_bot?branch=master)](https://travis-ci.org/nhomar/thecodinglove_bot)

# The Coding Love Bot.
Search GIF's by the text in stack The Coding love.

# Usar y depurar:

- Populate the database.

```shell
$ git clone git@bitbucket.org:vauxoo/thecodinglove.git
$ cd thecodinglove
$ sudo pip install -r requirements.txt
$ ./run
... wait until finish it take like 5 minutes ...
```

- Install this package

```bash
$ git clone git@bitbucket.org:vauxoo/thecodinglove_bot.git
$ cd thecodinglove_bot
$ sudo pip install -r requirements.txt
$ export TELEGRAM_TOKEN=YOUR_TOKEN#GENERATED-https://github.com/eternnoir/pyTelegramBotAPI#prerequisites
$ ./bot_inline.py
```

# Debug:

```bash
$ python bot_inline.py
```

# ROADMAP:

1. docker file to deploy.
2. setup.py
3. The search should return randomized but organized results.

