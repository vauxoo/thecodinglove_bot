FROM       ubuntu:14.04
MAINTAINER Nhomar Hernandez <nhomar@vauxoo.com>
RUN apt-get update && apt-get install -y python-pip python-dev build-essential libssl-dev libffi-dev 
# Installing the package always.
RUN pip install https://bitbucket.org/vauxoo/thecodinglove_bot/get/master.zip  --upgrade
ENTRYPOINT ["tcl_bot", "--debug"]
